package com.timkj.wxtemplatemessage.config;

import lombok.Data;

import java.io.File;
import java.io.IOException;

/**
 * @author : liji
 * @date : 2020-10-01 11:58
 */
@Data
public class BasicConfig {
    private String logPath;

    public BasicConfig(){
        try {
            String rootPath = new File("").getCanonicalPath();
            logPath = rootPath + "/log/";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
