package com.timkj.wxtemplatemessage.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.timkj.wxtemplatemessage.entity.FaMsgEntity;
import com.timkj.wxtemplatemessage.utils.log.LogUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;

/**
 * @author : liji
 * @date : 2020-08-25 16:12
 */
@Slf4j
public class BasicService {

    /**
     * @param entity fa_msg
     * @return json
     */
    public JSONObject getSendMessageEntityData(FaMsgEntity entity){
        SimpleDateFormat dateFormat=new SimpleDateFormat("YYYY-MM-dd");
        JSONObject jsonObject = new JSONObject();
        JSONObject first = new JSONObject();
        first.put("value",entity.getMsg_title());
        first.put("color","#173177");
        JSONObject keyword1 = new JSONObject();
        keyword1.put("value",entity.getSchool_name());
        keyword1.put("color","#173177");
        JSONObject keyword2 = new JSONObject();
        keyword2.put("value",entity.getAdmin_name());
        keyword2.put("color","#173177");
        JSONObject keyword3 = new JSONObject();
        keyword3.put("value", dateFormat.format(new Date()));
        keyword3.put("color","#173177");
        JSONObject keyword4 = new JSONObject();
        keyword4.put("value","请同学们点击查看通知详情，并点击“确认已读”按钮。");
        keyword4.put("color","#173177");
        jsonObject.put("first",first);
        jsonObject.put("keyword1",keyword1);
        jsonObject.put("keyword2",keyword2);
        jsonObject.put("keyword3",keyword3);
        jsonObject.put("keyword4",keyword4);
        return jsonObject;
    }

    /**
     * 微信公众号获取用户openid列表
     * @param accessToken 微信token
     * @return 用户openid列表
     */
    public ArrayList<String> getUserList(String accessToken){
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://api.weixin.qq.com/cgi-bin/user/get?access_token="+accessToken+"&next_openid=";
        ResponseEntity responseEntity = restTemplate.getForEntity(url,JSONObject.class);
        JSONObject json = (JSONObject) responseEntity.getBody();
        assert json != null;
        HashMap mapTypes = null;
        if (json.getJSONObject("data")!=null){
             mapTypes = JSON.parseObject(json.getJSONObject("data").toJSONString(),HashMap.class);
        }
        assert mapTypes != null;
        JSONArray openidArray = (JSONArray) mapTypes.get("openid");
        return (ArrayList<String>) JSON.parseArray(openidArray.toJSONString(), String.class);
    }


    /**
     * 获取access_token
     * @param appId
     * @param appSecret
     * @return access_token
     */
    public String getAccessToken(String appId,String appSecret){
        RestTemplate restTemplate = new RestTemplate();
        String grantType = "client_credential";
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type="+ grantType +"&appid="+appId+"&secret="+appSecret;
        ResponseEntity responseEntity = restTemplate.getForEntity(url, JSONObject.class);
        JSONObject json = (JSONObject) responseEntity.getBody();
        assert json != null;
        LogUtil.save(LogUtil.info(this.getClass().getName(),"access_token : "  + json.getString("access_token")));
        log.info("access_token : "  + json.getString("access_token"));
        return json.getString("access_token");
    }
}
