package com.timkj.wxtemplatemessage.service;

import com.timkj.wxtemplatemessage.dto.SendByOpenidDto;
import com.timkj.wxtemplatemessage.dto.SendByTaskidDto;
import com.timkj.wxtemplatemessage.dto.SendMassDto;
import com.timkj.wxtemplatemessage.entity.ResultVo;

import java.util.ArrayList;

/**
 * @author : liji
 * @date : 2020-08-25 15:30
 */
public interface WxTemplateMessageService {
    /**
     * 群发模板消息
     */
    ArrayList<ResultVo> sendMass(String accessToken,SendMassDto dto);

    /**
     * 通过openid发送模板消息
     */
    ArrayList<ResultVo> sendByOpenid(String accessToken,SendByOpenidDto dto);

    /**
     * 通过任务id发送模板消息
     */
    ArrayList<ResultVo> sendByTaskid(String accessToken,SendByTaskidDto dto);

    /**
     * 获取token
     */
    String getToken(String appId,String appSecret);
}
