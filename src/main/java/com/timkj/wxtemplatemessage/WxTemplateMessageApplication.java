package com.timkj.wxtemplatemessage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WxTemplateMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(WxTemplateMessageApplication.class, args);
    }

}
