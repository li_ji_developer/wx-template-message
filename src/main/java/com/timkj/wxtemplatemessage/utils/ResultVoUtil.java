package com.timkj.wxtemplatemessage.utils;

import com.timkj.wxtemplatemessage.entity.ResultVo;
import com.timkj.wxtemplatemessage.enums.ResultEnums;

/**
 * @Author: beifengtz
 * @Desciption: http响应的工具类
 * @Date: Created in 10:28 2018/5/12
 * @Modified By:
 */
public class ResultVoUtil {

    public ResultVo success(Object object){
        return new ResultVo(ResultEnums.SUCCESS,object);
    }

}
