package com.timkj.wxtemplatemessage.utils.log;

/**
 * @author : liji
 * @date : 2020-10-01 12:27
 */
public enum LogEnums {
    //追踪
    trace,
    //信息
    info,
    //调试
    debug,
    //警告
    warn,
    //错误
    error
}
