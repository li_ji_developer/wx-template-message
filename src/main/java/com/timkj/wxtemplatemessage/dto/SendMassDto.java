package com.timkj.wxtemplatemessage.dto;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * @author : liji
 * @date : 2020-08-25 16:04
 */
@Data
@Component
public class SendMassDto {
    /**
     * 微信公众号appId
     */
    @NotBlank(message = "appId不能为空")
    private String appId;
    /**
     * 微信公众号appSecret
     */
    @NotBlank(message = "appSecret不能为空")
    private String appSecret;
    /**
     * 微信公众号跳转连接url
     */
    @NotBlank(message = "url不能为空")
    private String url;
    /**
     * 微信模板消息id
     */
    @NotBlank(message = "template_id不能为空")
    private String template_id;
    /**
     * 去掉的openid列表
     */
    @NotNull(message = "list不能为空")
    private ArrayList<String> list;
    /**
     * 微信模板消息数据
     */
    @NotNull(message = "data不能为null")
    private JSONObject data;
}
