package com.timkj.wxtemplatemessage.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author : liji
 * @date : 2020-08-26 09:29
 */
@Data
@Component
public class SendByTaskidDto {
    /**
     * 微信appId
     */
    @NotBlank(message = "appId不能为空")
    private String appId;
    /**
     * 微信appSecret
     */
    @NotBlank(message = "appSecret不能为空")
    private String appSecret;
    /**
     * 任务id
     */
    @NotNull(message = "taskId不能为空")
    private int taskId;
}
