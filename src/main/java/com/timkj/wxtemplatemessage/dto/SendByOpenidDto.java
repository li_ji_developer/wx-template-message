package com.timkj.wxtemplatemessage.dto;

import com.timkj.wxtemplatemessage.entity.SendMessageEntity;
import lombok.Data;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * @author : liji
 * @date : 2020-08-25 15:30
 */
@Data
@Component
public class SendByOpenidDto {
    /**
     * 微信公众号appId
     */
    @NotBlank(message = "appId不能为空")
    private String appId;
    /**
     * 微信公众号appSecret
     */
    @NotBlank(message = "appSecret不能为空")
    private String appSecret;
    /**
     * 微信模板消息数据
     */
    @NotNull(message = "msgData不能为null")
    private ArrayList<SendMessageEntity> msgData;
}
