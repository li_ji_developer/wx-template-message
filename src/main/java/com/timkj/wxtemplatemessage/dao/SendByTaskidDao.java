package com.timkj.wxtemplatemessage.dao;

import com.timkj.wxtemplatemessage.entity.FaBaseUserEntity;
import com.timkj.wxtemplatemessage.entity.FaMsgEntity;
import com.timkj.wxtemplatemessage.entity.FaMsgItemEntity;
import com.timkj.wxtemplatemessage.entity.WxTemplateMessageTokenEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

/**
 * @author : liji
 * @date : 2020-08-26 09:38
 */
@Repository
@Mapper
public interface SendByTaskidDao {
    /**
     * @param Taskid 任务id
     * @return fa_msg
     */
    ArrayList<FaMsgEntity> SelectFaMsgByTaskid(int Taskid);

    /**
     * @param Taskid 任务id
     * @return  fa_msg_item
     */
    ArrayList<FaMsgItemEntity> SelectFaMsgItemByTaskid(int Taskid);

    /**
     * 回写消息任务状态
     * @param status FaMsgEntity
     * @return int
     */
    int UpdataStatus(FaMsgEntity status);

    /**
     * 回写消息item是否送达
     * @param entity FaMsgItemEntity
     * @return int
     */
    int UpdataIsSuccess(FaMsgItemEntity entity);

    /**
     * 根据学院搜索fa_baseuser
     * @param xy 学院
     * @return fa_baseuser
     */
    ArrayList<FaBaseUserEntity> SelectBaseUserByXy(String xy);

    /**
     * 回写 未送达原因
     * @param entity FaMsgItemEntity
     * @return int
     */
    int UpdataResult(FaMsgItemEntity entity);

    /**
     * 插入access_token 7000秒
     */
    int InsertAccessToken(WxTemplateMessageTokenEntity entity);

    /**
     * 查询表中最后一条记录 根据id排序
     * 查询token 是否过期
     */
    WxTemplateMessageTokenEntity SelectAccessToken();
}
