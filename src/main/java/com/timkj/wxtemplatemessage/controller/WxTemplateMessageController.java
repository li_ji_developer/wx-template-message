package com.timkj.wxtemplatemessage.controller;

import com.timkj.wxtemplatemessage.dto.SendByOpenidDto;
import com.timkj.wxtemplatemessage.dto.SendByTaskidDto;
import com.timkj.wxtemplatemessage.dto.SendMassDto;
import com.timkj.wxtemplatemessage.entity.ResultVo;
import com.timkj.wxtemplatemessage.service.WxTemplateMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;

/**
 * @author : liji
 * @date : 2020-08-25 15:21
 */
@Validated
@RestController
@RequestMapping("/timkj")
public class WxTemplateMessageController {
    private WxTemplateMessageService wxTemplateMessageService;

    @Autowired
    public WxTemplateMessageController(WxTemplateMessageService wxTemplateMessageService) {
        this.wxTemplateMessageService = wxTemplateMessageService;
    }

    /** 群发 */
    @RequestMapping(value = "/SendMass",method = RequestMethod.POST)
    public ArrayList<ResultVo> sendMass(@RequestBody @Valid SendMassDto dto){
        return wxTemplateMessageService.sendMass(wxTemplateMessageService.getToken(dto.getAppId(),dto.getAppSecret()),dto);
    }

    /** 通过openid发送 */
    @RequestMapping(value = "/SendByOpenid",method = RequestMethod.POST)
    public ArrayList<ResultVo> sendByOpenid(@RequestBody @Valid SendByOpenidDto dto){
        return wxTemplateMessageService.sendByOpenid(wxTemplateMessageService.getToken(dto.getAppId(),dto.getAppSecret()),dto);
    }

    /** 任务id发送 */
    @RequestMapping(value = "/SendByTaskid",method = RequestMethod.POST)
    public ArrayList<ResultVo> sendByTaskid(@RequestBody @Valid SendByTaskidDto dto){
        return wxTemplateMessageService.sendByTaskid(wxTemplateMessageService.getToken(dto.getAppId(),dto.getAppSecret()),dto);
    }
}
