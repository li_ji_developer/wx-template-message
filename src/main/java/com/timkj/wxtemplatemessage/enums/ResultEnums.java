package com.timkj.wxtemplatemessage.enums;


/**
 * @Author: beifengtz
 * @Desciption: 返回状态枚举类
 * @Date: Created in 19:20 2018/5/7
 * @Modified By:
 */
public enum ResultEnums {

    /**
     * 操作成功
     */
    SUCCESS(0,"成功");

    /**
     * code值
     */
    private int code;
    /**
     * message值
     */
    private String msg;

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    ResultEnums(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}
