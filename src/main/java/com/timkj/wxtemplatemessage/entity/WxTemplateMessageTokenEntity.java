package com.timkj.wxtemplatemessage.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @author : liji
 * @date : 2020-09-24 13:31
 */
@Data
@Component
public class WxTemplateMessageTokenEntity implements Serializable {
    private int id;
    private String access_token;
    private long create_time;
    private long out_time;
}
