package com.timkj.wxtemplatemessage.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author : liji
 * @date : 2020-08-26 09:48
 */
@Data
@Component
public class FaMsgEntity {
    private int id;
    private enum Type{
        /** 模板消息.*/
        modemsg,
        /** 短信消息.*/
        msg
    }
    private enum Status{
        /** 创建.*/
        创建,
        /** 发送.*/
        发送,
        /** 完成.*/
        完成,
        /** 失败.*/
        失败
    }
    private String type;
    private String status;
    private String url;
    private String msg_title;
    private String template_id;
    private int createtime;
    private int updatetime;
    private String msg_content;
    private int a_admin_id;
    private String admin_name;
    private String school_name;
}
