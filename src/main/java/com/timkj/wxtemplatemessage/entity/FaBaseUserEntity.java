package com.timkj.wxtemplatemessage.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author : liji
 * @date : 2020-08-31 08:54
 */
@Data
@Component
public class FaBaseUserEntity {
    private int id;
    private String name;
    private String sfzh;
    private String xh;
    private String school;
    private String xy;
    private String zy;
    private String bj;
    private String mz;
    private String csny;
    private String zzmm;
    private String xb;
    private int schoolid;
    private int yb_userid;
    private int user_id;
    private int enteryear;
    private String wx_openid;
    private String wx_avatar;
    private String wx_nickname;
    private String wx_appid;
    private String yb_token;
    private int updatetime;
}
