package com.timkj.wxtemplatemessage.entity;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author : liji
 * @date : 2020-08-26 09:48
 */
@Data
@Component
public class FaMsgItemEntity {
    private int id;
    private int msg_id;
    private int user_id;
    private enum Type{
        微信,
        短信
    }
    private String type;
    private String v_info;
    private enum Status{
        未读,
        已读未确认,
        已读
    }
    private String status;
    private int createtime;
    private String endtime;
    private String confirmtime;
    private String isSuccess;
    private String result;
}
