package com.timkj.wxtemplatemessage.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author : liji
 * @date : 2020-08-25 16:05
 */
@Data
@Component
public class SendMessageEntity {
    /**
     * 微信openid
     */
    private String touser;
    /**
     * 微信模板消息id
     */
    private String template_id;
    /**
     * 模板消息跳转连接
     */
    private String url;
    /**
     * 微信模板消息数据
     */
    private JSONObject data;
}
