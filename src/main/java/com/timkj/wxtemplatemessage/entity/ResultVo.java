package com.timkj.wxtemplatemessage.entity;

import com.timkj.wxtemplatemessage.enums.ResultEnums;
import lombok.Data;

/**
 * @author : liji
 * @date : 2020-08-25 15:34
 */
@Data
public class ResultVo {
    /** 错误码. */
    private Integer code;

    /** 提示信息. */
    private String msg;

    /** 微信openid. */
    private String openid;

    /** 具体内容. */
    private Object data;

    public ResultVo() {
    }

    public ResultVo(ResultEnums resultEnums, Object data){
        this.code = resultEnums.getCode();
        this.msg = resultEnums.getMsg();
        this.openid = getOpenid();
        this.data = data;
    }
}
