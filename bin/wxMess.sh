#! /bin/bash
#
# kconfig: 345 80 20
# description: start the integration service
#
# Source function library
#. /etc/rc.d/init.d/functions
JAVA_HOME=/usr/java/jdk1.8.0_121
APP_HOME=/www/WxMessage
JAR_FILE=wx-template-message-0.0.1-SNAPSHOT.jar
APP_PID="$APP_HOME/bin/WxMessage.pid"
case "$1" in
start)
    echo "Starting WxMessage Service..."
    if [ -f $APP_PID ] ; then
        echo "WxMessage Service has already started. Please delete $APP_PID if otherwise."
    else
        bash -c "cd $APP_HOME && nohup $JAVA_HOME/bin/java -Dfile.encoding=utf-8 -server -Xms256m -Xmx256m -Xmn128m -XX:SurvivorRatio=2 -XX:PermSize=96m -XX:MaxPermSize=256m -Xss256k -XX:-UseAdaptiveSizePolicy -XX:MaxTenuringThreshold=15 -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:+HeapDumpOnOutOfMemoryError -jar $JAR_FILE >/dev/null 2>&1 & echo \$! > $APP_PID"
        echo "WxMessage Service started"
    fi
    ;;
stop)
    echo "Stopping WxMessage Service..."
    if [ -f $APP_PID ] ; then
        PID=`cat $APP_PID`
        pkill -P $PID
        if kill -9 $PID ; then
            echo "WxMessage Service stopped"
        else
            echo "WxMessage Service could not be stopped"
        fi
        rm -f $APP_PID
    fi
    ;;
*)
    echo "Usage: $prog {start|stop}"
    ;;
esac
exit 0
